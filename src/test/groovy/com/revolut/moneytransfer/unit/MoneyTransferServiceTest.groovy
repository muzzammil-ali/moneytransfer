package com.revolut.moneytransfer.unit

import com.revolut.moneytransfer.model.Account
import com.revolut.moneytransfer.model.Transaction
import com.revolut.moneytransfer.repository.RelationalAccountDao
import com.revolut.moneytransfer.repository.RelationalTransactionDao
import com.revolut.moneytransfer.request.MoneyTransferRequest
import com.revolut.moneytransfer.response.Error
import com.revolut.moneytransfer.services.MoneyTransferService
import spock.lang.Specification
import spock.lang.Unroll

import javax.persistence.NoResultException

class MoneyTransferServiceTest extends Specification {
    MoneyTransferService service
    RelationalTransactionDao transactionDao
    RelationalAccountDao accountDao

    def setup(){
        transactionDao = Mock(RelationalTransactionDao)
        accountDao = Mock(RelationalAccountDao)
        service = new MoneyTransferService(transactionDao, accountDao)
    }

    def "shouldTransferMoney"(){
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300)
                .senderAccountNumber(111).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        Account senderAccount = Account.builder().account_id(11).accountNumber(111)
                .sortcode("11-11-11").balance(1000).build()
        Account receiverAccount = Account.builder().account_id(22).accountNumber(222)
                .sortcode("22-22-22").balance(100).build()

        when:
        service.transferMoney(request)

        then:
        noExceptionThrown()
        1 * accountDao.getByAccountNumberAndSortcode(request.senderAccountNumber, request.senderSortcode) >> senderAccount
        1 * accountDao.getByAccountNumberAndSortcode(request.receiverAccountNumber, request.receiverSortcode) >> receiverAccount
        1 * transactionDao.create({ Transaction t -> (t.amount == 300 & t.receiver.account_id == 22 & t.sender.account_id == 11
                & t.sender.balance == 700 & t.receiver.balance == 400 )})
    }

    def "should return error when sender account not found"(){
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300)
                .senderAccountNumber(111).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        when:
        Error response = service.transferMoney(request)

        then:
        1 * accountDao.getByAccountNumberAndSortcode(request.senderAccountNumber,
                request.senderSortcode) >> { throw new NoResultException() }
        0 *_

        and:
        response.code == 400002
        response.message == "Sender account does not exist"
    }

    def "should return error when receiver account not found"(){
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300)
                .senderAccountNumber(111).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()
        Account senderAccount = Account.builder().account_id(11).accountNumber(111)
                .sortcode("11-11-11").balance(500).build()

        when:
        Error response = service.transferMoney(request)

        then:
        1 * accountDao.getByAccountNumberAndSortcode(request.senderAccountNumber,
                request.senderSortcode) >> senderAccount
        1 * accountDao.getByAccountNumberAndSortcode(request.receiverAccountNumber,
                request.receiverSortcode) >> { throw new NoResultException() }
        0 *_

        and:
        response.code == 400003
        response.message == "receiver account does not exist"
    }

    def "should return error when sender account does not have enough funds"() {
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300)
                .senderAccountNumber(111).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        Account senderAccount = Account.builder().account_id(11).accountNumber(111)
                .sortcode("11-11-11").balance(200).build()

        when:
        Error response = service.transferMoney(request)

        then:
        1 * accountDao.getByAccountNumberAndSortcode(request.senderAccountNumber,
                request.senderSortcode) >> senderAccount

        0 *_

        and:
        response.code == 400001
        response.message == "Insufficient funds in sender account"
    }

    def "should return error when sender sortcode is missing"() {
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300)
                .senderAccountNumber(111)
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        def response = service.transferMoney(request)
        when:
        response

        then:
        noExceptionThrown()
        0 *_

        and:
        response.code == 400004
        response.message == "Insufficient account detail"
    }

    def "should return error when sender accountNumber is missing"() {
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(300).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        def response = service.transferMoney(request)
        when:
        response

        then:
        noExceptionThrown()
        0 *_

        and:
        response.code == 400004
        response.message == "Insufficient account detail"
    }

    @Unroll
    def "should return error when amount is #amount"() {
        given:
        MoneyTransferRequest request = MoneyTransferRequest.builder()
                .amount(amount)
                .senderAccountNumber(333).senderSortcode("11-11-11")
                .receiverAccountNumber(222).receiverSortcode("22-22-22")
                .build()

        Account senderAccount = Account.builder().account_id(11).accountNumber(111)
                .sortcode("11-11-11").balance(200).build()

        when:
        def response = service.transferMoney(request)

        then:
        1 * accountDao.getByAccountNumberAndSortcode(request.senderAccountNumber,
                request.senderSortcode) >> senderAccount

        then:
        noExceptionThrown()
        0 *_

        and:
        response.code == 400005
        response.message == "Invalid transfer amount"

        where:
        amount << [0, -100]
    }
}
