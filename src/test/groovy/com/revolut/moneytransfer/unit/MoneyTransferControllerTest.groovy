package com.revolut.moneytransfer.unit

import com.revolut.moneytransfer.request.MoneyTransferRequest
import com.revolut.moneytransfer.response.Error
import com.revolut.moneytransfer.response.MoneyTransferFailedException
import com.revolut.moneytransfer.services.MoneyTransferController
import com.revolut.moneytransfer.services.MoneyTransferService
import spock.lang.Specification

class MoneyTransferControllerTest extends Specification {
    public static final String SORTCODE = "11-11-11"
    MoneyTransferController moneyTransferController
    MoneyTransferService moneyTransferService

    def setup(){
        moneyTransferService = Mock()
        moneyTransferController = new MoneyTransferController(moneyTransferService)
    }

    def "should transfer money from one account to another" () {
        given:
        MoneyTransferRequest payload = MoneyTransferRequest.builder()
                .senderAccountNumber(1313).senderSortcode(SORTCODE)
                .receiverAccountNumber(1212).receiverSortcode(SORTCODE)
                .amount(200).build()

        when:
        def response = moneyTransferController.transferMoney(payload)

        then:
        response == "money transferred successfully"

        and:
        1 * moneyTransferService.transferMoney(payload) >> null
        0 *_
    }

    def "should throw exception when error is returned by service" () {
        given:
        MoneyTransferRequest payload = MoneyTransferRequest.builder()
                .senderAccountNumber(1313)
                .receiverAccountNumber(1212).receiverSortcode(SORTCODE)
                .amount(200).build()

        when:
        moneyTransferController.transferMoney(payload)

        then:
        def e = thrown(MoneyTransferFailedException)
        e.errorMessage == Error.NOT_ENOUGH_FUNDS.message
        e.errorCode == Error.NOT_ENOUGH_FUNDS.code

        and:
        1 * moneyTransferService.transferMoney(payload) >> Error.NOT_ENOUGH_FUNDS
        0 *_
    }
}
