package com.revolut.moneytransfer.repository

import com.revolut.moneytransfer.model.Account
import com.revolut.moneytransfer.model.Customer
import com.revolut.moneytransfer.model.Transaction
import spock.lang.Specification

class RelationalTransactionDaoTest extends Specification {

    RelationalTransactionDao dao = new RelationalTransactionDao()
    RelationalAccountDao accountDao = new RelationalAccountDao()

    def shouldUpdateAccountsAfterSuccessfulTransaction(){
        given:
        def senderAccount = Account.builder().sortcode("11-11-11")
                .accountNumber(111).balance(3000).build()

        def receiverAccount = Account.builder().balance(2000)
                .accountNumber(1313).sortcode("22-22-22").build()

        createAccount(senderAccount)
        createAccount(receiverAccount)

        senderAccount.setBalance(1000)

        Transaction transaction = Transaction.builder().id(12).amount(500)
                .sender(senderAccount)
                .receiver(receiverAccount)
                .build()
        println "accountDao = ${accountDao.getAll()}"

        when:
        def transactionId = dao.create(transaction)

        then:
        noExceptionThrown()
        Transaction result = dao.getById(transactionId)
        result.id == transactionId
        result.amount == 500
        result.getSender().balance == 1000
    }

    private createAccount(Account account) {
        accountDao.create(account)
    }
}
