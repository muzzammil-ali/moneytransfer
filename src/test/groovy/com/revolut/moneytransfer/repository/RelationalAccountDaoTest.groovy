package com.revolut.moneytransfer.repository

import com.revolut.moneytransfer.model.Account
import com.revolut.moneytransfer.model.Customer
import com.revolut.moneytransfer.model.Transaction
import spock.lang.Specification

import javax.persistence.NoResultException

class RelationalAccountDaoTest extends Specification {
    RelationalAccountDao dao = new RelationalAccountDao()

    def shouldCreateAccount() {
        given:
        def account = Account.builder().sortcode("11-11-11")
                .accountNumber(123456).balance(3000).build()

        when:
        def accountId = dao.create(account)

        then:
        noExceptionThrown()
        Account result = (Account)dao.getById(accountId)
        result.account_id == accountId
        result.balance == account.balance
        result.sortcode == account.sortcode
        result.accountNumber == account.accountNumber
    }

    def "should get account by account number and sortcode" (){
        given:
        def account = Account.builder().sortcode("22-22-22")
                .accountNumber(33333).balance(3000).build()
        dao.create(account)

        when:
        def result = dao.getByAccountNumberAndSortcode(33333, "22-22-22")

        then:
        result
        result.balance == 3000
    }

    def "should throw exception when no account found for account detail" (){
        when:
        def result = dao.getByAccountNumberAndSortcode(11, "xx-xx-xx")

        then:
        thrown(NoResultException)
    }
}
