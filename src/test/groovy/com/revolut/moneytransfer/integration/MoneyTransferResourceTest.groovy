package com.revolut.moneytransfer.integration

import com.mashape.unirest.http.HttpResponse
import com.mashape.unirest.http.Unirest
import com.revolut.moneytransfer.ApplicationMain
import com.revolut.moneytransfer.model.Account
import com.revolut.moneytransfer.repository.RelationalAccountDao
import groovy.json.JsonOutput
import groovy.json.JsonSlurper;
import spock.lang.Specification

class MoneyTransferResourceTest extends Specification {
    public static final String SORT_CODE_1 = "11-11-11"
    private int port

    def setup() {
        port = ApplicationMain.startServer()
    }

    def cleanup(){
        ApplicationMain.stopServer()
        try {
            Thread.sleep(2000)
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt()
        }
    }

    def "heath is working" () {
        when:
        HttpResponse<String> health = Unirest.get("http://localhost:" + port + "/health").asString()

        then:
        health.status == 200
        !health.body.isEmpty()
    }


    def "should return 200 when transfer money from one account to another" () {
        given:
        def sender = createAccount(1500)
        def receiver = createAccount(5500)

        def payload = JsonOutput.toJson(['senderAccountNumber': sender.accountNumber, 'senderSortcode': SORT_CODE_1,
                                         'receiverAccountNumber': receiver.accountNumber, 'receiverSortcode': SORT_CODE_1,
                                         'amount': 500])

        println payload
        when:
        HttpResponse<String> health = Unirest.post(transferMoneyEndpoint).body(payload)asString()

        then:
        health.status == 200
        getAccount(sender.account_id).balance == 1000
        getAccount(receiver.account_id).balance == 6000

        cleanup:
        deleteAccount(sender)
        deleteAccount(receiver)

    }

    def "should return error(400) when sender account does not have sufficient funds" () {
        given:
        def sender = createAccount(100)
        def receiver = createAccount(5500)
        def payload = JsonOutput.toJson(['senderAccountNumber': sender.accountNumber, 'senderSortcode': SORT_CODE_1,
                                         'receiverAccountNumber': receiver.accountNumber, 'receiverSortcode': SORT_CODE_1,
                                         'amount': 500])

        when:
        HttpResponse<String> health = Unirest.post(transferMoneyEndpoint).body(payload)asString()

        then: "check sender and receiver account balance has not changed"
        health.status == 400
        getAccount(sender.account_id).balance == sender.balance
        getAccount(receiver.account_id).balance == receiver.balance

        and: "error details are returned"
        getBody(health).errorCode == 400001
        getBody(health).errorMessage == 'Insufficient funds in sender account'

        cleanup:
        deleteAccount(sender)
        deleteAccount(receiver)
    }

    def "should return error(400) when sender account does not exist" () {
        given:

        def receiver = createAccount(5500)
        def payload = JsonOutput.toJson(['senderAccountNumber': 000, 'senderSortcode': SORT_CODE_1,
                                         'receiverAccountNumber': receiver.accountNumber, 'receiverSortcode': SORT_CODE_1,
                                         'amount': 500])

        when:
        HttpResponse<String> health = Unirest.post(transferMoneyEndpoint).body(payload)asString()

        then: "check sender and receiver account balance has not changed"
        health.status == 400
        getAccount(receiver.account_id).balance == receiver.balance

        and: "error details are returned"
        getBody(health).errorCode == 400002
        getBody(health).errorMessage == 'Sender account does not exist'

        cleanup:
        deleteAccount(receiver)
    }

    def "should return error(400) when receiver account does not exist" () {
        given:
        def sender = createAccount(1000)
        def payload = JsonOutput.toJson(['senderAccountNumber': sender.accountNumber, 'senderSortcode': SORT_CODE_1,
                                         'receiverAccountNumber': 0000, 'receiverSortcode': SORT_CODE_1,
                                         'amount': 500])

        when:
        HttpResponse<String> health = Unirest.post(transferMoneyEndpoint).body(payload)asString()

        then: "check sender and receiver account balance has not changed"
        health.status == 400
        getAccount(sender.account_id).balance == sender.balance

        and: "error details are returned"
        getBody(health).errorCode == 400003
        getBody(health).errorMessage == 'receiver account does not exist'

        cleanup:
        deleteAccount(sender)
    }

    private Account createAccount(int balance) {
        def account = Account.builder()
                .accountNumber(random).sortcode(SORT_CODE_1)
                .balance(balance).build()
        createAccount(account)
        account
    }

    private Account getDummyAccount() {
        Account.builder().account_id(00)
                .accountNumber(000).sortcode(SORT_CODE_1)
                .balance(5000).build()
    }

    private deleteAccount(Account senderAccount) {
        new RelationalAccountDao().delete(senderAccount)
    }

    def getBody(HttpResponse<String> health) {
        new JsonSlurper().parseText(health.body)
    }

    private createAccount(Account account) {
        new RelationalAccountDao().create(account)
    }

    private Account getAccount(accountId) {
        new RelationalAccountDao().getById(accountId)
    }

    private String getTransferMoneyEndpoint() {
        "http://localhost:" + port + "/transfermoney"
    }

    private int getRandom() {
        int min = 10
        int max = 1000
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }
}
