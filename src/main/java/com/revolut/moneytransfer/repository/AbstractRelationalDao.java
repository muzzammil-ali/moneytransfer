package com.revolut.moneytransfer.repository;

import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.model.Transaction;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.Serializable;
import java.util.List;

public abstract class AbstractRelationalDao< T extends Serializable> {
    static SessionFactory sessionFactory = new Configuration().configure()
                .buildSessionFactory();
    private Class< T > clazz;

    public AbstractRelationalDao(Class< T > clazzToSet) {
//        sessionFactory = new Configuration().configure()
//                .buildSessionFactory();
        clazz = clazzToSet;
    }

    public int create(T t) {
        Session session = sessionFactory.openSession();
        session.beginTransaction();
        int result = (int)session.save(t);
        session.getTransaction().commit();
        return result;
    }

    public T getById(int id) {
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        T t = session.get(clazz, id);
        session.close();
        return t;
    }

    public List<T> getAll() {
        Session session = sessionFactory.openSession();
        @SuppressWarnings("unchecked")
        List<T> list = session.createQuery("from " + clazz.getName()).list();
        session.close();
        return list;
    }

    public void delete(Account account) {
        Session session = sessionFactory.openSession();
        session.delete(account);
        session.close();
    }
}
