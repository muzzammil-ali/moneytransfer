package com.revolut.moneytransfer.repository;

import com.revolut.moneytransfer.model.Transaction;

public class RelationalTransactionDao extends AbstractRelationalDao {

    public RelationalTransactionDao (){
        super(Transaction.class);
    }
}
