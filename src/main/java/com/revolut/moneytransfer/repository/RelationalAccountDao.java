package com.revolut.moneytransfer.repository;

import com.revolut.moneytransfer.model.Account;
import org.hibernate.Session;
import org.hibernate.query.Query;

public class RelationalAccountDao extends AbstractRelationalDao {

    public RelationalAccountDao(){
        super(Account.class);
    }

    public Account getByAccountNumberAndSortcode(int accountNumber, String sortcode) {
        Session session = sessionFactory.openSession();
        Query query = session.createQuery(
                "FROM Account A WHERE A.accountNumber = :account_number AND A.sortcode = :sortcode");
        query.setParameter("account_number", accountNumber);
        query.setParameter("sortcode", sortcode);
        @SuppressWarnings("unchecked")
        Account account = (Account) query.getSingleResult();
        session.close();
        return account;
    }
}
