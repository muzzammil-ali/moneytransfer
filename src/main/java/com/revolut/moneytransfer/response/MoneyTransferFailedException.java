package com.revolut.moneytransfer.response;

import lombok.Getter;
import lombok.ToString;

@ToString(of = {"errorCode", "errorMessage"})
public class MoneyTransferFailedException extends RuntimeException {

    @Getter
    private int errorCode;
    @Getter
    private String errorMessage;

    public MoneyTransferFailedException(Error error) {
        super(error.getMessage());
        this.errorCode = error.getCode();
        this.errorMessage = error.getMessage();
    }
}
