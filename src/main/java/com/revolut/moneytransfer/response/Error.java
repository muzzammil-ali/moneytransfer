package com.revolut.moneytransfer.response;

import lombok.Getter;

@Getter
public enum Error {
    NOT_ENOUGH_FUNDS(400, 400001, "Insufficient funds in sender account"),
    SENDER_ACCOUNT_INVALID(400, 400002, "Sender account does not exist"),
    RECEIVER_ACCOUNT_INVALID(400, 400003, "receiver account does not exist"),
    INSUFFICIENT_ACCOUNT_DETAIL(400, 400004, "Insufficient account detail"),
    INVALID_AMOUNT(400, 400005, "Invalid transfer amount");

    private int httpStatus;
    private int code;
    private String message;

    Error(int httpStatus, int code, String message) {
        this.httpStatus = httpStatus;
        this.code = code;
        this.message = message;
    }
}
