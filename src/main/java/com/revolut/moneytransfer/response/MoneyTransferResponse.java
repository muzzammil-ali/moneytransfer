package com.revolut.moneytransfer.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

@AllArgsConstructor
@Builder
@Getter
public class MoneyTransferResponse {
    private int statusCode;
    private Error error;
}
