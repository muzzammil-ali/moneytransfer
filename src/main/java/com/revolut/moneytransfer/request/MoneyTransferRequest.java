package com.revolut.moneytransfer.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@ToString
@Builder
public class MoneyTransferRequest {
    private Integer senderAccountNumber;
    private String senderSortcode;
    private Integer receiverAccountNumber;
    private String receiverSortcode;
    private double amount;
}
