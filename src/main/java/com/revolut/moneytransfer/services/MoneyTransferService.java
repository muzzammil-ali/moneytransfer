package com.revolut.moneytransfer.services;

import static com.revolut.moneytransfer.response.Error.INSUFFICIENT_ACCOUNT_DETAIL;
import static com.revolut.moneytransfer.response.Error.INVALID_AMOUNT;
import static com.revolut.moneytransfer.response.Error.NOT_ENOUGH_FUNDS;
import static com.revolut.moneytransfer.response.Error.RECEIVER_ACCOUNT_INVALID;
import static com.revolut.moneytransfer.response.Error.SENDER_ACCOUNT_INVALID;

import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.model.Transaction;
import com.revolut.moneytransfer.repository.RelationalAccountDao;
import com.revolut.moneytransfer.repository.RelationalTransactionDao;
import com.revolut.moneytransfer.request.MoneyTransferRequest;
import com.revolut.moneytransfer.response.Error;
import lombok.NoArgsConstructor;

import javax.persistence.NoResultException;
import java.util.Date;
@NoArgsConstructor
public class MoneyTransferService {
    private RelationalTransactionDao transactionDao;
    private RelationalAccountDao accountDao;

    public MoneyTransferService(RelationalTransactionDao transactionDao, RelationalAccountDao accountDao) {
        this.transactionDao = transactionDao;
        this.accountDao = accountDao;
    }

    public Error transferMoney(final MoneyTransferRequest request) {
        if (requiredFieldsAreMissing(request)) return INSUFFICIENT_ACCOUNT_DETAIL;

        Account senderAccount = getAccount(request.getSenderAccountNumber(), request.getSenderSortcode());
        Error error = validateSenderAccount(request, senderAccount);

        if(error != null) {
            return error;
        }

        Account receiverAccount = getAccount(request.getReceiverAccountNumber(), request.getReceiverSortcode());
        if (receiverAccount == null) {
            return RECEIVER_ACCOUNT_INVALID;
        }

        senderAccount.setBalance(senderAccount.getBalance() - request.getAmount());
        receiverAccount.setBalance(receiverAccount.getBalance() + request.getAmount());
        Transaction transaction = Transaction.builder().amount(request.getAmount()).sender(senderAccount)
                .receiver(receiverAccount).transactionDate(new Date()).build();

        transactionDao.create(transaction);
        return null;
    }

    private Error validateSenderAccount(MoneyTransferRequest request, Account senderAccount) {
        if(senderAccount == null) {
            return SENDER_ACCOUNT_INVALID;
        }

        if(request.getAmount() <=0) {
            return INVALID_AMOUNT;
        }

        if(senderAccount.getBalance() < request.getAmount()) {
            return NOT_ENOUGH_FUNDS;
        }

        return null;
    }

    private boolean requiredFieldsAreMissing(MoneyTransferRequest request) {
        return request.getSenderSortcode() == null || request.getReceiverSortcode() == null ||
                request.getSenderAccountNumber() == null || request.getReceiverAccountNumber() == null;
    }

    private Account getAccount(int senderAccountNumber, String senderSortCode) {
        try {
            return accountDao.getByAccountNumberAndSortcode(senderAccountNumber,
                    senderSortCode);
        } catch(NoResultException e){
            return null;
        }
    }
}
