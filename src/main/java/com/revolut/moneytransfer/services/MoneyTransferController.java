package com.revolut.moneytransfer.services;

import com.revolut.moneytransfer.request.MoneyTransferRequest;
import com.revolut.moneytransfer.response.Error;
import com.revolut.moneytransfer.response.MoneyTransferFailedException;

public class MoneyTransferController {
    MoneyTransferService moneyTransferService;

    public MoneyTransferController(MoneyTransferService moneyTransferService) {
        this.moneyTransferService = moneyTransferService;
    }

    public String transferMoney(MoneyTransferRequest payload){

        Error error = moneyTransferService.transferMoney(payload);

        if(error != null) {
            throw new MoneyTransferFailedException(error);
        }
        return "money transferred successfully";
    }
}
