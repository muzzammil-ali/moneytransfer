package com.revolut.moneytransfer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;
import java.util.List;

@Builder
@Entity
@Data
@Table(name = "account",
        uniqueConstraints = {@UniqueConstraint(columnNames = {"sortcode", "account_number"})})
@AllArgsConstructor
@NoArgsConstructor
public class Account implements Serializable {
    @Id
    @GeneratedValue
    private int account_id;

    @Column(name="sortcode", nullable = false)
    private String sortcode;

    @Column(name="account_number", nullable = false)
    private int accountNumber;

    @Column(name="balance")
    private double balance;
}
