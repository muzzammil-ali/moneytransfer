package com.revolut.moneytransfer.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

@Table(name = "transaction")
@ToString
@Data
@Builder
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Transaction implements Serializable{
    @Id
    @GeneratedValue
    private int id;

    @Column(name="amount")
    private double amount;

    @JoinColumn(name="sender_account_id")
    @OneToOne(cascade = CascadeType.ALL)
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Account sender;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="receiver_account_id")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Account receiver;

    @Column(name="transaction_date")
    Date transactionDate;
}
