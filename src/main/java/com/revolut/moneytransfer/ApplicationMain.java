package com.revolut.moneytransfer;

import static spark.Spark.before;
import static spark.Spark.exception;
import static spark.Spark.get;
import static spark.Spark.path;
import static spark.Spark.post;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.revolut.moneytransfer.model.Account;
import com.revolut.moneytransfer.repository.RelationalAccountDao;
import com.revolut.moneytransfer.repository.RelationalTransactionDao;
import com.revolut.moneytransfer.request.MoneyTransferRequest;
import com.revolut.moneytransfer.response.MoneyTransferFailedException;
import com.revolut.moneytransfer.services.MoneyTransferController;
import com.revolut.moneytransfer.services.MoneyTransferService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.ResponseTransformer;
import spark.Spark;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ApplicationMain {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationMain.class);

    private static final Gson GSON = new Gson();
    private static final ResponseTransformer JSON_TRANSFORMER = GSON::toJson;
    private static final String JSON = "application/json";

    public static void main(String[] Args) {
        startServer();
    }

    public static int startServer() {
        Spark.init();

        initializeRoutes();

        exception(JsonSyntaxException.class, ApplicationMain::handleInvalidInput);
        LOG.debug("Created exception handlers");

        Spark.awaitInitialization();
        LOG.debug("Ready");
        return Spark.port();
    }

    private static void initializeRoutes() {
        before((request, response) -> response.type(JSON));
        // This initlization should be done though dependency injection
        RelationalAccountDao accountDao = new RelationalAccountDao();
        MoneyTransferService moneyTransferService = new MoneyTransferService(new RelationalTransactionDao(), accountDao);
        MoneyTransferController moneyTransferController = new MoneyTransferController(moneyTransferService);

        path("/health", () -> get("", (req, res) -> "healthy"));
        //For testing purpose to create accounts
        post("/account", (request, response) -> accountDao.create(
                GSON.fromJson(request.body(), Account.class)));

        post("/transfermoney", (request, response) -> moneyTransferController.transferMoney(
                    GSON.fromJson(request.body(), MoneyTransferRequest.class)));

        exception(MoneyTransferFailedException.class, ApplicationMain::handleErrors);

        LOG.debug("Initialised routes");
    }

    private static void handleErrors(MoneyTransferFailedException e, Request request, Response response) {
        response.status(400);
        response.type(JSON);
        Map<Object, Object> payload = new HashMap<>();
        payload.put("errorCode", e.getErrorCode());
        payload.put("errorMessage", e.getErrorMessage());
        response.body(GSON.toJson(payload));
    }

    private static void handleInvalidInput(Exception e, Request request, Response response) {
        response.status(400);
        response.type(JSON);
        response.body(GSON.toJson(Collections.singletonMap("error", e.getMessage())));
    }

    /**
     * For testing, as we want to start and stop the server.
     */
    public static void stopServer() {
        LOG.debug("Asking server to stop");
        Spark.stop();
    }
}
