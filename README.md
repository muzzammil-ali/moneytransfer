`Money Transfer Application`
Money transfer app transfers money from one bank account to another.
The application runs locally on http://localhost:4567/account

Money Transfer:

endpoint /moneytransfer
sample payload
{"senderAccountNumber":<SENDER_ACCOUNT_NUMBER>,
 "senderSortcode":"<SENDER_SORTCODE>",
 "receiverAccountNumber":<RECEIVER_ACCOUNT_NUMBER>,
 "receiverSortcode":"<RECEIVER_SORTCODE",
 "amount":<AMOUNT_TO_SEND}'

| Error code | Http Status | Error Message |
| ---        | ---       | ---       |
| 40001      | 400       | Insufficient funds in sender account |
| 40002      | 400       | Sender account does not exist |
| 40003      | 400       | receiver account does not exist |
| 40004      | 400       | Insufficient account detail |
| 40005      | 400       | Invalid transfer amount |

sample payload
Run application:
to run the application run the following command in the terminal
```java -jar build/libs/moneyTransfer-1.0.jar ```

Demo:
to transfer money run the demo script
```./demo.sh```

To create accounts for testing there is an endpoint /accounts. This is created to test the moneytransfer functionality. Idealy it should be separated from production code but due to lack of time it has been put in production code.