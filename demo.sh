#!/usr/bin/env bash

pause() {
	local msg=$1
  echo -e "\033[u\033[1m$msg\033[u\033[0m [Return]"
  read
}

pause 'create sender account'
curl -d '{"accountNumber": "102", "sortcode": "11-11-11", "balance": 1000}' -H "Content-Type: application/json" -X POST http://localhost:4567/account

pause 'create receiver account'
curl -d '{"accountNumber": "103", "sortcode": "22-22-22", "balance": 5000}' -H "Content-Type: application/json" -X POST http://localhost:4567/account

pause 'transfer money'
curl -d '{"senderAccountNumber":102,"senderSortcode":"11-11-11","receiverAccountNumber":103,"receiverSortcode":"11-11-11","amount":500}' -H "Content-Type: application/json" -X POST http://localhost:4567/transfermoney

